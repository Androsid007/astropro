package com.horoscope.astrobot.astropro.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.horoscope.astrobot.astropro.Network.RetrofitCall;
import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.session.SessionManager;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdListener;
import com.yrkfgo.assxqx4.AdView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Login extends Fragment implements View.OnClickListener,AdListener {
    private TextView forgetPassword;
    private EditText userName, password;
    private Button login;
    private String email, pass, options;
    Fragment fragment;
    SessionManager sessionManager;
    FragmentTransaction ft;
    AdView adView;
    private RetrofitCall call = new RetrofitCall();


    public Login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        AdConfig.setAppId(336319);
        AdConfig.setApiKey("1346132145416878338");

        AdConfig.setTestMode(false);
        AdConfig.setPlacementId(0);
        AdView.setAdListener(this);
        adView=(AdView) view.findViewById(R.id.myAdView);
        if(adView!=null)
            adView.loadAd();



        getAllID(view);

        return view;
    }

    public void getAllID(View view) {
        sessionManager = new SessionManager(getContext());
        login = (Button) view.findViewById(R.id.loginButton);
        login.setOnClickListener(this);
        forgetPassword = (TextView) view.findViewById(R.id.forgetPassword);
        forgetPassword.setVisibility(View.GONE);
        userName = (EditText) view.findViewById(R.id.userName);
        password = (EditText) view.findViewById(R.id.password);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                Log.d("Retrofit", " checkLogin()");
                email = userName.getText().toString().trim();
                pass = password.getText().toString().trim();
                userName.setText("");
                password.setText("");
                Log.d("Retrofit", " checkLogin()" + email + " " + pass);
                call.checkLogin(getActivity(), email, pass);

                break;

            case R.id.forgetPassword:
                break;


        }
    }

    @Override
    public void onError(ErrorType errorType, String s) {

    }

    @Override
    public void onAdLoading() {

    }

    @Override
    public void onAdLoaded() {

    }

    @Override
    public void onAdExpanded() {

    }

    @Override
    public void onAdClicked() {

    }

    @Override
    public void onAdClosed() {

    }

    @Override
    public void onAdCached(AdConfig.AdType adType) {

    }
}
