package com.horoscope.astrobot.astropro.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class  CommonUtility {
    public enum CallerFunction {
       GET_SEARCH_CATEGORY_ITEM,PRODUCT_ITEM_SEARCH_MASTER,LOGIN_SUCCESS_CHAT,BY_DEFAULT_RES


    }
    public enum HTTP_REQUEST_TYPE{
        GET,POST
    }
    public enum StoreType {
        NONE, GOOGLE_PLAY
    }
    public static final int RETROFIT_TIMEOUT=30000;

    public static boolean isNetworkAvailable(Context _context) {
        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
