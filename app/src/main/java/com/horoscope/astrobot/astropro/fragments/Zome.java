package com.horoscope.astrobot.astropro.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.horoscope.astrobot.astropro.Adapters.DemoAdapter;
import com.horoscope.astrobot.astropro.Network.RetrofitCall;
import com.horoscope.astrobot.astropro.Network.RetrofitTask;
import com.horoscope.astrobot.astropro.Network.RetrofitTaskListener;
import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.common.CommonUtility;
import com.horoscope.astrobot.astropro.common.ServerConfigStage;
import com.horoscope.astrobot.astropro.model.Demo;
import com.horoscope.astrobot.astropro.session.SessionManager;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;

import retrofit.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Zome extends Fragment implements RetrofitTaskListener<Demo>, com.yrkfgo.assxqx4.AdListener {
    ProgressDialog progressDialog;


    private RecyclerView mrRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private DemoAdapter mChatAdapter;
    //private ChatFirstAdapter fChatAdapter;
    private ImageView mSend;
    private EditText mMessageEditText;
    //List<InformationModel> mDataList;
    Demo mDataList;


    private String chatMsg, astroMsg,email;
    private SessionManager session;
    private TextView timeTxt;
    private RetrofitCall call = new RetrofitCall();
    private long remainTime;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";
    private Main main;
    AdView adView;





    public Zome() {
        // Required empty public constructor
    }

    public static Zome newInstance(Context context,int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);
        Zome sampleFragment = new Zome();
        sampleFragment.setArguments(args);
        FragmentTransaction ft =((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("fragHome");
        ft.commit();


        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        session = new SessionManager(getActivity().getApplicationContext());
        AdConfig.setAppId(336319);
        AdConfig.setApiKey("1346132145416878338");

         AdConfig.setTestMode(false);
        AdConfig.setPlacementId(0);
        AdView.setAdListener(this);

        adView=(AdView) view.findViewById(R.id.myAdView);
        if(adView!=null)
            adView.loadAd();



        getAllId(view);
        allClicks();
        if (session.isLoggedIn()){
            SharedPreferences shared = getActivity().getSharedPreferences("MyPrefs", MODE_PRIVATE);
            email = (shared.getString("emailKey", ""));
            timeTxt.setVisibility(View.VISIBLE);

        }else {
            timeTxt.setVisibility(View.GONE);
        }
        return view;
    }

    private void allClicks() {
        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == mSend) {
                        chatMsg = mMessageEditText.getText().toString().trim();
                    if (chatMsg.isEmpty()) {

                    } else {
                        String[] words = chatMsg.split("/");
                        if (words.length == 2) {
                            mMessageEditText.setText("");
                            String url = String.format(ServerConfigStage.GET_USER_INFO(),"PRASHANT SIVASTAVA&DOB=01-05-1990");
                            RetrofitTask task = new RetrofitTask<Demo>(Zome.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_SEARCH_CATEGORY_ITEM, url, getActivity());
                            task.execute();

                         } else if (words.length == 1) {
                            mMessageEditText.setText("");
                            String url = String.format(ServerConfigStage.GET_USER_fIRST_RESPONSE(),"hi","0");
                            RetrofitTask task = new RetrofitTask<Demo>(Zome.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_SEARCH_CATEGORY_ITEM, url, getActivity());
                            task.execute();
                         }

                    }
                }

            }
        });
    }

    private void getAllId(View view) {
        timeTxt = (TextView)view.findViewById(R.id.time);
        mMessageEditText = (EditText) view.findViewById(R.id.edtvw);
        mrRecyclerView = (RecyclerView) view.findViewById(R.id.recvw);
        mSend = (ImageView) view.findViewById(R.id.btnvw);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mrRecyclerView.setLayoutManager(mLinearLayoutManager);

    }




    @Override
    public void onStart(){
        super.onStart();
       }







    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }



    @Override
    public void onRetrofitTaskComplete(Response<Demo> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (_callerFunction == CommonUtility.CallerFunction.GET_SEARCH_CATEGORY_ITEM) {
                    if (response.body()!=null) {


                        mDataList = response.body();



                            mChatAdapter = new DemoAdapter(getContext(),mDataList, chatMsg, mLinearLayoutManager);
    mrRecyclerView.setAdapter(mChatAdapter);
    mChatAdapter.notifyDataSetChanged();

                    }

                }
            }
        }
    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {

        //stopProgress();
        Toast.makeText(getActivity(), "Fail to load Data", Toast.LENGTH_LONG).show();

        getActivity().finish();

    }









    @Override
    public void onDestroy() {
        stopProgress();
        super.onDestroy();
    }

    @Override
    public void onError(ErrorType errorType, String s) {

    }

    @Override
    public void onAdLoading() {

    }

    @Override
    public void onAdLoaded() {

    }

    @Override
    public void onAdExpanded() {

    }

    @Override
    public void onAdClicked() {

    }

    @Override
    public void onAdClosed() {

    }

    @Override
    public void onAdCached(AdConfig.AdType adType) {

    }
}