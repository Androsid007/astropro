package com.horoscope.astrobot.astropro.Adapters;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.model.Demo;
import com.horoscope.astrobot.astropro.model.InformationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rahul on 03/02/17.
 */

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.ViewHolder> {
    Context mContext;
    //private InformationModel mDataList;
   Demo mDataList1;
   LinearLayoutManager mLayoutManager;
    String userMsg;
    TextToSpeech t1;
    ArrayList<Demo> anv = new ArrayList<>();





    public DemoAdapter(Context mContext,  Demo mDataList, String userMsg, LinearLayoutManager mLayoutManager) {
        this.mContext = mContext;
        this.mDataList1 = mDataList;
        this.mLayoutManager = mLayoutManager;
        this.userMsg = userMsg;
        anv.add(mDataList1);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_chat, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

         String[] words = userMsg.split("/");
        if (words.length == 2) {


            String mString = mDataList1.getName() + "\n" +
                    mDataList1.getDate() + "\n" +
                    mDataList1.getDestinyNumber() + "\n" +
                    mDataList1.getRadicalNum() + "\n" +
                    mDataList1.getNameNumber() + "\n" +
                    mDataList1.getEvilNum() + "\n" +
                    mDataList1.getFavColor() + "\n" +
                    mDataList1.getFavDay() + "\n" +
                    mDataList1.getFavGod() + "\n" +
                    mDataList1.getFavMantra() + "\n" +
                    mDataList1.getFavMetal() + "\n" +
                    mDataList1.getFavStone() + "\n" +
                    mDataList1.getFavSubstone() + "\n" +
                    mDataList1.getFriendlyNum() + "\n" +
                    mDataList1.getEvilNum() + "\n" +
                    mDataList1.getRadicalNum();
            if (!mString.isEmpty()) {
                holder.astroMsg.setText("ASTRO BOT:-" + mString);

                t1=new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.UK);
                        }
                        if (status == TextToSpeech.SUCCESS){
                            t1.setLanguage(Locale.ENGLISH);
                            // t1.setLanguage(Locale.ENGLISH);
                            String toSpeak =  holder.astroMsg.getText().toString();
                            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                        }
                    }
                });
               /* String toSpeak = holder.astroMsg.getText().toString();
                 t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
*/
            }
            if (userMsg != null) {
                holder.userMsg.setText("USER:-" + userMsg);
                t1=new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.UK);
                        }
                      /*  if (status == TextToSpeech.SUCCESS){
                            t1.setLanguage(Locale.ENGLISH);
                             String toSpeak =  holder.userMsg.getText().toString();
                            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                        }*/
                    }
                });

            }
        } else if (words.length == 1){

           String mString = mDataList1.getReply();
            if (!mString.isEmpty()) {
                holder.astroMsg.setText("ASTRO BOT:-" + mString);
            }
            if (userMsg != null) {
                holder.userMsg.setText("USER:-" + userMsg);
            }
        }
    }

    @Override
    public int getItemCount() {

        return anv.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView userMsg, astroMsg;

        public ViewHolder(View itemView) {
            super(itemView);
            userMsg = (TextView) itemView.findViewById(R.id.user);
            astroMsg = (TextView) itemView.findViewById(R.id.astrobot);


        }
    }
}