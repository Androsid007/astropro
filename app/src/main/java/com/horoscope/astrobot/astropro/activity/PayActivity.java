package com.horoscope.astrobot.astropro.activity;

import in.verse.ipayy.crypto.CryptoException;
import in.verse.mpayment.PaymentActivity;
import in.verse.mpayment.PaymentService;
import in.verse.mpayment.enums.Currency;
import in.verse.mpayment.enums.DiscountType;
import in.verse.mpayment.request.Item;
import in.verse.mpayment.request.ItemDetail;
import in.verse.mpayment.response.FailedPaymentResponse;
import in.verse.mpayment.response.SuccessPaymentResponse;

import java.math.BigDecimal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.horoscope.astrobot.astropro.Network.RequestGenerator;
import com.horoscope.astrobot.astropro.R;

public class PayActivity extends Activity {
    private static final String applicationKey = "p-X6GcWSa4JhnBcOoyL3wg";
    private static final String merchantKey = "8zuueXpmpJCZ6LL5Xce5mA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

    }

    protected void onResume() {
        super.onResume();
        Button payButton = (Button) findViewById(R.id.button1);
        //payButton.setText(getString(R.string.btn_pay_now));
        payButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ItemDetail itemDetail = new ItemDetail("ItemTest", "TestGame",
                        new BigDecimal("1.0"), BigDecimal.ZERO,
                        DiscountType.AMOUNT);
                Item item = new Item(itemDetail, Currency.INR);
                final String requestId = RequestGenerator.getNewRequestId(PayActivity.this);
                Intent paymentIntent = null;
                try {
                    paymentIntent = PaymentService.getInstance()
                            .getPaymentIntent(
                                    requestId,
                                    merchantKey, applicationKey,
                                    "9958246062", "Airtel",
                                    PayActivity.this, null, item);
                } catch (CryptoException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                startActivityForResult(paymentIntent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            switch (resultCode) {
                case PaymentService.RESULT_OK: {
                    Intent intent = new Intent(this, SuccessActivity.class);
                    SuccessPaymentResponse successPaymentResponse = (SuccessPaymentResponse) data
                            .getExtras().getSerializable(
                                    PaymentService.SUCCESS_PAYMENT_RESPONSE);
                    Log.d("S",
                            successPaymentResponse.toString());
                    intent.putExtra(PaymentService.SUCCESS_PAYMENT_RESPONSE,
                            successPaymentResponse);
                    startActivity(intent);
                    break;
                }
                case PaymentService.RESULT_FAILURE: {
                    Intent intent = new Intent(this, FailureActivity.class);
                    FailedPaymentResponse failedPaymentResponse = (FailedPaymentResponse) data
                            .getExtras().getSerializable(
                                    PaymentService.FAILED_PAYMENT_RESPONSE);
                    Log.d("Failed Payment Response",
                            failedPaymentResponse.toString());
                    intent.putExtra(PaymentService.FAILED_PAYMENT_RESPONSE,
                            failedPaymentResponse);
                    startActivity(intent);
                    break;
                }
                case PaymentService.RESULT_CANCELED: {
                    if (data != null) {
                        Intent intent = new Intent(this, CanceledActivity.class);
                        Item item = (Item) data.getExtras().getSerializable(
                                PaymentService.ITEM);
                        Log.d("Result Cancelled", "Cancelled");
                        intent.putExtra("RESPONSE",
                                "cancelled");
                        intent.putExtra(PaymentService.ITEM, item);
                        startActivity(intent);
                        break;
                    }
                }
                default:
                    break;
            }
        }
    }


}
