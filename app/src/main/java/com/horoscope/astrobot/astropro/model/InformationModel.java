package com.horoscope.astrobot.astropro.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Arif on 03/02/17.
 */
public class InformationModel implements Parcelable {
    public InformationModel() {
    }


    protected InformationModel(Parcel in) {
        name = in.readString();
        date = in.readString();
        destiny_number = in.readString();
        radical_number = in.readString();
        name_number = in.readString();
        evil_num = in.readString();
        fav_color = in.readString();
        fav_day = in.readString();
        fav_god = in.readString();
        fav_mantra = in.readString();
        fav_metal = in.readString();
        fav_stone = in.readString();
        fav_substone = in.readString();
        friendly_num = in.readString();
        neutral_num = in.readString();
        radical_num = in.readString();
        radical_ruler = in.readString();
        status = in.readString();
        query = in.readString();
        reply = in.readString();



    }

    public static final Creator<InformationModel> CREATOR = new Creator<InformationModel>() {
        @Override
        public InformationModel createFromParcel(Parcel in) {
            return new InformationModel(in);
        }

        @Override
        public InformationModel[] newArray(int size) {
            return new InformationModel[size];
        }
    };
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDestiny_number() {
        return destiny_number;
    }

    public void setDestiny_number(String destiny_number) {
        this.destiny_number = destiny_number;
    }

    public String getRadical_number() {
        return radical_number;
    }

    public void setRadical_number(String radical_number) {
        this.radical_number = radical_number;
    }

    public String getName_number() {
        return name_number;
    }

    public void setName_number(String name_number) {
        this.name_number = name_number;
    }

    public String getEvil_num() {
        return evil_num;
    }

    public void setEvil_num(String evil_num) {
        this.evil_num = evil_num;
    }

    public String getFav_color() {
        return fav_color;
    }

    public void setFav_color(String fav_color) {
        this.fav_color = fav_color;
    }

    public String getFav_day() {
        return fav_day;
    }

    public void setFav_day(String fav_day) {
        this.fav_day = fav_day;
    }

    public String getFav_god() {
        return fav_god;
    }

    public void setFav_god(String fav_god) {
        this.fav_god = fav_god;
    }

    public String getFav_mantra() {
        return fav_mantra;
    }

    public void setFav_mantra(String fav_mantra) {
        this.fav_mantra = fav_mantra;
    }

    public String getFav_metal() {
        return fav_metal;
    }

    public void setFav_metal(String fav_metal) {
        this.fav_metal = fav_metal;
    }

    public String getFav_stone() {
        return fav_stone;
    }

    public void setFav_stone(String fav_stone) {
        this.fav_stone = fav_stone;
    }

    public String getFav_substone() {
        return fav_substone;
    }

    public void setFav_substone(String fav_substone) {
        this.fav_substone = fav_substone;
    }

    public String getFriendly_num() {
        return friendly_num;
    }

    public void setFriendly_num(String friendly_num) {
        this.friendly_num = friendly_num;
    }

    public String getNeutral_num() {
        return neutral_num;
    }

    public void setNeutral_num(String neutral_num) {
        this.neutral_num = neutral_num;
    }

    public String getRadical_num() {
        return radical_num;
    }

    public void setRadical_num(String radical_num) {
        this.radical_num = radical_num;
    }

    public String getRadical_ruler() {
        return radical_ruler;
    }

    public void setRadical_ruler(String radical_ruler) {
        this.radical_ruler = radical_ruler;
    }

    private String name;
    private String date;
    private String destiny_number;
    private String radical_number;
    private String name_number;
    private String evil_num;
    private String fav_color;
    private String fav_day;
    private String fav_god;
    private String fav_mantra;
    private String fav_metal;
    private String fav_stone;
    private String fav_substone;
    private String friendly_num;
    private String neutral_num;
    private String radical_num;
    private String radical_ruler;
    private String status;
    private String query;
    private String reply;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(date);
        dest.writeString(destiny_number);
        dest.writeString(radical_number);
        dest.writeString(name_number);
        dest.writeString(evil_num);
        dest.writeString(fav_color);
        dest.writeString(fav_day);
        dest.writeString(fav_god);
        dest.writeString(fav_mantra);
        dest.writeString(fav_metal);
        dest.writeString(fav_stone);
        dest.writeString(fav_substone);
        dest.writeString(friendly_num);
        dest.writeString(neutral_num);
        dest.writeString(radical_num);
        dest.writeString(radical_ruler);
        dest.writeString(status);
        dest.writeString(query);
        dest.writeString(reply);



    }
}
