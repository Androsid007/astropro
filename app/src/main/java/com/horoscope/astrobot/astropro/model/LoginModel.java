package com.horoscope.astrobot.astropro.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif on 06/02/17.
 */

public class LoginModel {
    @SerializedName("status")
    private String status;

    public LoginModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("msg")
    private String message;
}
