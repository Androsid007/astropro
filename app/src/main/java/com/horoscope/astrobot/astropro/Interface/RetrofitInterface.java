package com.horoscope.astrobot.astropro.Interface;


import com.horoscope.astrobot.astropro.model.Balance;
import com.horoscope.astrobot.astropro.model.InformationModel;
import com.horoscope.astrobot.astropro.model.LoginModel;
import com.horoscope.astrobot.astropro.model.LoginSuccess;
import com.horoscope.astrobot.astropro.model.Registration;
import com.horoscope.astrobot.astropro.model.RemainigTime;
import com.horoscope.astrobot.astropro.model.mMessageModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Arif on 03/02/17.
 */
public interface RetrofitInterface {
    @GET("/webservices/astrodata.php")
    Call<InformationModel> getUserInfo(@Query("NAME") String name, @Query("DOB") String dob);

    @GET("/webservices/chat.php")
    Call<InformationModel> getUserHello(@Query("QUERY") String name);

    @GET("/webservices/savedob.php")
    Call<mMessageModel> sendMessage(@Query("EMAIL") String name, @Query("DOB") String dob, @Query("LOC") String loc);

    @GET("/webservices/login.php")
    Call<LoginModel> login(@Query("EMAIL") String email, @Query("PASSWORD") String password);

    @GET("webservices/checkbal.php")
    Call<Balance> balance(@Query("EMAIL") String email);

    @GET("webservices/registration.php")
    Call<Registration> register(@Query("NAME") String name, @Query("EMAIL") String email, @Query("CREDIT") String credit, @Query("AMOUNT") String amount);

    @GET("/webservices/chat1.php")
    Call<LoginSuccess> getFirstMessage();

    @GET("/webservices/astrology.php")
    Call<InformationModel> loginComplete(@Query("EMAIL") String email,  @Query("OPTIONS") String option);

    @GET("/webservices/update_talktime.php")
    Call<RemainigTime> postRemainingTime(@Query("EMAIL")String email, @Query("MINUTES") String minutes);
}
