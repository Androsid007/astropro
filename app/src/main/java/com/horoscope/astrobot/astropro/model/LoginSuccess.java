package com.horoscope.astrobot.astropro.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif on 21/02/17.
 */

public class LoginSuccess {
    @SerializedName("status")
    private String status;

    @SerializedName("reply")
    private String reply;

    public LoginSuccess() {
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public LoginSuccess(String status, String reply) {
        this.status = status;
        this.reply = reply;


    }
}
