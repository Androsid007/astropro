package com.horoscope.astrobot.astropro.common;

import android.support.design.BuildConfig;
import android.util.Log;

/**
 * Created by rahul on 14-06-2016.
 */
public class DeBug {
    static boolean toShowLog = BuildConfig.DEBUG;
    public static void showLog(String TAG,String Message){
        if(TAG!=null)
            if(toShowLog)
                Log.i(TAG, Message);
    }
}
