package com.horoscope.astrobot.astropro.model;

/**
 * Created by hp1 on 2/25/2017.
 */



        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class LoginCh {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("destiny_number")
    @Expose
    private Integer destinyNumber;
    @SerializedName("radical_number")
    @Expose
    private Integer radicalNumber;
    @SerializedName("name_number")
    @Expose
    private Integer nameNumber;
    @SerializedName("evil_num")
    @Expose
    private String evilNum;
    @SerializedName("fav_color")
    @Expose
    private String favColor;
    @SerializedName("fav_day")
    @Expose
    private String favDay;
    @SerializedName("fav_god")
    @Expose
    private String favGod;
    @SerializedName("fav_mantra")
    @Expose
    private String favMantra;
    @SerializedName("fav_metal")
    @Expose
    private String favMetal;
    @SerializedName("fav_stone")
    @Expose
    private String favStone;
    @SerializedName("fav_substone")
    @Expose
    private String favSubstone;
    @SerializedName("friendly_num")
    @Expose
    private String friendlyNum;
    @SerializedName("neutral_num")
    @Expose
    private String neutralNum;
    @SerializedName("radical_num")
    @Expose
    private String radicalNum;
    @SerializedName("radical_ruler")
    @Expose
    private String radicalRuler;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reply")
    @Expose
    private String reply;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDestinyNumber() {
        return destinyNumber;
    }

    public void setDestinyNumber(Integer destinyNumber) {
        this.destinyNumber = destinyNumber;
    }

    public Integer getRadicalNumber() {
        return radicalNumber;
    }

    public void setRadicalNumber(Integer radicalNumber) {
        this.radicalNumber = radicalNumber;
    }

    public Integer getNameNumber() {
        return nameNumber;
    }

    public void setNameNumber(Integer nameNumber) {
        this.nameNumber = nameNumber;
    }

    public String getEvilNum() {
        return evilNum;
    }

    public void setEvilNum(String evilNum) {
        this.evilNum = evilNum;
    }

    public String getFavColor() {
        return favColor;
    }

    public void setFavColor(String favColor) {
        this.favColor = favColor;
    }

    public String getFavDay() {
        return favDay;
    }

    public void setFavDay(String favDay) {
        this.favDay = favDay;
    }

    public String getFavGod() {
        return favGod;
    }

    public void setFavGod(String favGod) {
        this.favGod = favGod;
    }

    public String getFavMantra() {
        return favMantra;
    }

    public void setFavMantra(String favMantra) {
        this.favMantra = favMantra;
    }

    public String getFavMetal() {
        return favMetal;
    }

    public void setFavMetal(String favMetal) {
        this.favMetal = favMetal;
    }

    public String getFavStone() {
        return favStone;
    }

    public void setFavStone(String favStone) {
        this.favStone = favStone;
    }

    public String getFavSubstone() {
        return favSubstone;
    }

    public void setFavSubstone(String favSubstone) {
        this.favSubstone = favSubstone;
    }

    public String getFriendlyNum() {
        return friendlyNum;
    }

    public void setFriendlyNum(String friendlyNum) {
        this.friendlyNum = friendlyNum;
    }

    public String getNeutralNum() {
        return neutralNum;
    }

    public void setNeutralNum(String neutralNum) {
        this.neutralNum = neutralNum;
    }

    public String getRadicalNum() {
        return radicalNum;
    }

    public void setRadicalNum(String radicalNum) {
        this.radicalNum = radicalNum;
    }

    public String getRadicalRuler() {
        return radicalRuler;
    }

    public void setRadicalRuler(String radicalRuler) {
        this.radicalRuler = radicalRuler;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

}