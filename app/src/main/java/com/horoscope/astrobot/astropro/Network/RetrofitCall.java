package com.horoscope.astrobot.astropro.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.horoscope.astrobot.astropro.Interface.MyInterface;
import com.horoscope.astrobot.astropro.Interface.RetrofitInterface;

import com.horoscope.astrobot.astropro.Interface.TimeInterface;
import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.activity.NavigationActivity;

import com.horoscope.astrobot.astropro.fragments.LoginSuccessChat;
import com.horoscope.astrobot.astropro.model.Balance;
import com.horoscope.astrobot.astropro.model.InformationModel;
import com.horoscope.astrobot.astropro.model.LoginModel;
import com.horoscope.astrobot.astropro.model.Registration;
import com.horoscope.astrobot.astropro.model.RemainigTime;
import com.horoscope.astrobot.astropro.model.mMessageModel;
import com.horoscope.astrobot.astropro.session.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Arif on 03/02/17.
 */
public class RetrofitCall {

    boolean checkRegister = false;

    private SessionManager session;
    public static String balance;
    private FragmentTransaction ft;
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;


    RetrofitInterface mRetrofitInterface;
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://astrobot.co.in")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public void mLogin(final Context mContext, final MyInterface mMyInterface, String mName, String mDateOfBirth) {
        mRetrofitInterface = retrofit.create(RetrofitInterface.class);

        Call<InformationModel> call = mRetrofitInterface.getUserInfo(mName, mDateOfBirth);
        call.enqueue(new Callback<InformationModel>() {
            @Override
            public void onResponse(Call<InformationModel> call, Response<InformationModel> response) {

                Toast.makeText(mContext, response.body().getName(), Toast.LENGTH_SHORT).show();
                mMyInterface.myData("info", response.body());
            }

            @Override
            public void onFailure(Call<InformationModel> call, Throwable t) {

            }
        });
    }

    public void mLoginone(final Context mContext, final MyInterface mMyInterface, String mName) {
        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<InformationModel> call = mRetrofitInterface.getUserHello(mName);
        call.enqueue(new Callback<InformationModel>() {
            @Override
            public void onResponse(Call<InformationModel> call, Response<InformationModel> response) {

                Toast.makeText(mContext, response.body().getReply(), Toast.LENGTH_SHORT).show();
                mMyInterface.myData("info", response.body());
            }

            @Override
            public void onFailure(Call<InformationModel> call, Throwable t) {

            }
        });
    }

    public void mSendMessage(final Context mContext, final MyInterface mMyInterface, String Email, String mDateOfBirth, String loc) {
        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<mMessageModel> call = mRetrofitInterface.sendMessage(Email, mDateOfBirth, loc);
        call.enqueue(new Callback<mMessageModel>() {
            @Override
            public void onResponse(Call<mMessageModel> call, Response<mMessageModel> response) {

                Toast.makeText(mContext, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                mMyInterface.myData("info", response.body());
            }

            @Override
            public void onFailure(Call<mMessageModel> call, Throwable t) {

            }
        });
    }

    public void checkLogin(final Context context, final String email, String password) {
        session = new SessionManager(context);
        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
        Log.d("Retrofit", " checkLogin()");
        Call<LoginModel> call = mRetrofitInterface.login(email, password);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                String status = response.body().getStatus();
                if (status.equals("authorized")) {
                    session.setLogin(true);
                    sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("emailKey", email);
                    editor.apply();
                    NavigationActivity activity = (NavigationActivity) context;
                    FragmentManager fragmentManager = activity.getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    LoginSuccessChat fragment = new LoginSuccessChat();
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.commit();
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean registerUser(final Context context, String name, String email, String credit, String amount) {

        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<Registration> call = mRetrofitInterface.register(name, email, credit, amount);
        call.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {
                checkRegister = true;
                Toast.makeText(context, response.body().getStatus(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                checkRegister = false;
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();

            }
        });
        return checkRegister;
    }

    public void checkBalance(final Context context, final TimeInterface timeInterface, String email) {
        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<Balance> call = mRetrofitInterface.balance(email);
        call.enqueue(new Callback<Balance>() {
            @Override
            public void onResponse(Call<Balance> call, Response<Balance> response) {
                timeInterface.remainingTime("balance", response.body().getMessage());
                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Balance> call, Throwable t) {
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void sendTime(final Context context, final String email, final String minutes) {
        mRetrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<RemainigTime> call = mRetrofitInterface.postRemainingTime(email, minutes);
        call.enqueue(new Callback<RemainigTime>() {
            @Override
            public void onResponse(Call<RemainigTime> call, Response<RemainigTime> response) {
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<RemainigTime> call, Throwable t) {
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void startLoginFragmemt(Context context) {

    }
}

