package com.horoscope.astrobot.astropro.activity;

import in.verse.mpayment.PaymentService;
import in.verse.mpayment.response.SuccessPaymentResponse;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SuccessActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        SuccessPaymentResponse successPaymentResponse = (SuccessPaymentResponse) getIntent().getExtras().getSerializable(PaymentService.SUCCESS_PAYMENT_RESPONSE);
        TextView successpaymentMessage = new TextView(this);
        successpaymentMessage.setText("Congratulations!!! Your payment was successful.nTransaction ID:"+successPaymentResponse.getTransactionId()+"nAmount:"+successPaymentResponse.getAmountCharged());
        Button finishButton = new Button(this);
        finishButton.setText("Finish");
        finishButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(successpaymentMessage);
        linearLayout.addView(finishButton);

        setContentView(linearLayout);

    }
}
