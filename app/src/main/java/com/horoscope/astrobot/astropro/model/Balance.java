package com.horoscope.astrobot.astropro.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hp on 2/14/2017.
 */

public class Balance {
    @SerializedName("status")
    private String status;

    public Balance(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("msg")
    private String message;
}
