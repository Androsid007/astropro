package com.horoscope.astrobot.astropro.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CanceledActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        String failureReason = getIntent().getStringExtra("RESPONSE");
        TextView paymentFailureMessage = new TextView(this);
        paymentFailureMessage.setText(failureReason);

        Button finishButton = new Button(this);
        finishButton.setText("Finish");
        finishButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(paymentFailureMessage);
        linearLayout.addView(finishButton);
        setContentView(linearLayout);
    }
}
