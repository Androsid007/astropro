package com.horoscope.astrobot.astropro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.horoscope.astrobot.astropro.R;

/**
 * Created by Arif on 28/02/17.
 */

public class HomeOptions extends Fragment implements View.OnClickListener {
    private ImageButton chat, login, register, recharge;
    private Fragment fragment;
    private FragmentTransaction ft;

    public HomeOptions() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_screen, container, false);

        chat = (ImageButton) view.findViewById(R.id.chatButton);
        chat.setOnClickListener(this);

        login = (ImageButton) view.findViewById(R.id.loginButton);
        login.setOnClickListener(this);

        register = (ImageButton) view.findViewById(R.id.paymentButton);
        register.setOnClickListener(this);

        recharge = (ImageButton) view.findViewById(R.id.rechargeButton);
        recharge.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chatButton:
                fragment = new Zome();
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                break;

            case R.id.loginButton:
                fragment = new Login();
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                break;

            case R.id.paymentButton:
                fragment = new Payment();
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                break;

            case R.id.rechargeButton:
                fragment = new Services();
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                break;

        }

    }
}
