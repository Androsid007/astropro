package com.horoscope.astrobot.astropro.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horoscope.astrobot.astropro.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Services extends Fragment {


    public Services() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         return inflater.inflate(R.layout.fragment_services, container, false);
    }

}
