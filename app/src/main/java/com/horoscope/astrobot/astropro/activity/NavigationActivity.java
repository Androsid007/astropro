package com.horoscope.astrobot.astropro.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;


import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.fragments.Contact;
import com.horoscope.astrobot.astropro.fragments.HomeOptions;
import com.horoscope.astrobot.astropro.fragments.Login;
import com.horoscope.astrobot.astropro.fragments.Payment;
import com.horoscope.astrobot.astropro.fragments.Services;
import com.horoscope.astrobot.astropro.fragments.Zome;
import com.horoscope.astrobot.astropro.session.SessionManager;

public class NavigationActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Fragment fragment;
    private FragmentTransaction ft;
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        session = new SessionManager(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navigation);
        if (session.isLoggedIn()){
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_navigation_logout);
        }else if (!session.isLoggedIn()){
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_navigation);
        }
        navigationView.setItemIconTintList(null);

        if (navigationView != null) {
            setUpDrawerContent(navigationView);
        }


        fragment = new HomeOptions();
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment).commit();


    }

     private void setUpDrawerContent(final NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (item.getItemId()) {
                    case R.id.home:
                        toolbar.setTitle("Astrobot");
                        fragment = new Zome();
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.services:
                       toolbar.setTitle("Services");
                        fragment = new Services();
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.contact:
                        toolbar.setTitle("Contact");
                        fragment = new Contact();
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.payment:
                        toolbar.setTitle("Payment");
                        fragment = new Payment();
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                        break;
                    case R.id.login:
                        navigationView.getMenu().clear();
                        navigationView.inflateMenu(R.menu.menu_navigation_logout);
                        toolbar.setTitle("Login");
                        fragment = new Login();
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                        break;

                    case R.id.logout:
                        session.setLogin(false);
                        navigationView.getMenu().clear();
                        navigationView.inflateMenu(R.menu.menu_navigation);
                        Toast.makeText(NavigationActivity.this,"Logged Out",Toast.LENGTH_LONG).show();
                        fragment = new Login();
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
                }
                return true;
            }
        });
    }

    public void loadDwukomorowe() {
     /*  fragment = new Home();
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();

   */   fragment = new Zome();
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
