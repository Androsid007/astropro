package com.horoscope.astrobot.astropro.Adapters;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.model.InformationModel;
import com.horoscope.astrobot.astropro.model.LoginSuccess;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rahul on 03/02/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context mContext;
    List<InformationModel> mDataList = new ArrayList<>();
    List<LoginSuccess> loginList = new ArrayList<>();
    LinearLayoutManager mLayoutManager;
    String userMsg;
    TextToSpeech t1;
    int newMsg;



    public ChatAdapter(Context mContext, List<InformationModel> mDataList, String userMsg, LinearLayoutManager mLayoutManager) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutManager = mLayoutManager;
        this.userMsg = userMsg;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("Adapter", "onCreateViewHolder Called");
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_chat, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.d("Adapter", "onBindViewHolder Called");

        InformationModel mInformationModel = mDataList.get(position);


        String[] words = userMsg.split("=");
         if (words.length == 2) {


            String mString = mInformationModel.getName() + "\n" +
                    mInformationModel.getDate() + "\n" +
                    mInformationModel.getDestiny_number() + "\n" +
                    mInformationModel.getRadical_number() + "\n" +
                    mInformationModel.getName_number() + "\n" +
                    mInformationModel.getEvil_num() + "\n" +
                    mInformationModel.getFav_color() + "\n" +
                    mInformationModel.getFav_day() + "\n" +
                    mInformationModel.getFav_god() + "\n" +
                    mInformationModel.getFav_mantra() + "\n" +
                    mInformationModel.getFav_metal() + "\n" +
                    mInformationModel.getFav_stone() + "\n" +
                    mInformationModel.getFav_substone() + "\n" +
                    mInformationModel.getFriendly_num() + "\n" +
                    mInformationModel.getEvil_num() + "\n" +
                    mInformationModel.getRadical_num();
            if (!mString.isEmpty()) {
                holder.astroMsg.setText("ASTRO BOT:-" + mString);

                t1 = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.UK);
                        }
                        if (status == TextToSpeech.SUCCESS) {
                            t1.setLanguage(Locale.ENGLISH);
                            // t1.setLanguage(Locale.ENGLISH);
                            String toSpeak = holder.astroMsg.getText().toString();
                            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                        }
                    }
                });
               /* String toSpeak = holder.astroMsg.getText().toString();
                 t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
*/
            }
            if (userMsg != null) {
                holder.userMsg.setText("USER:-" + userMsg);
                t1 = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.UK);
                        }
                      /*  if (status == TextToSpeech.SUCCESS){
                            t1.setLanguage(Locale.ENGLISH);
                             String toSpeak =  holder.userMsg.getText().toString();
                            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                        }*/
                    }
                });

            }
        } else if (words.length == 1) {
            InformationModel mInformation = mDataList.get(position);

            String mString = mInformation.getReply();
            if (!mString.isEmpty()) {
                holder.astroMsg.setText("ASTRO BOT:-" + mString);
            }
            if (userMsg != null) {
                holder.userMsg.setText("USER:-" + userMsg);
            }
        }

    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView userMsg, astroMsg;

        public ViewHolder(View itemView) {
            super(itemView);
            userMsg = (TextView) itemView.findViewById(R.id.user);
            astroMsg = (TextView) itemView.findViewById(R.id.astrobot);


        }
    }
}