package com.horoscope.astrobot.astropro.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rahul on 03/02/17.
 */public class mMessageModel implements Parcelable {
    public mMessageModel() {
    }

    protected mMessageModel(Parcel in) {
        status = in.readString();
        msg = in.readString();
    }

    public static final Creator<mMessageModel> CREATOR = new Creator<mMessageModel>() {
        @Override
        public mMessageModel createFromParcel(Parcel in) {
            return new mMessageModel(in);
        }

        @Override
        public mMessageModel[] newArray(int size) {
            return new mMessageModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String status;
    private String msg;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(msg);
    }
}
