package com.horoscope.astrobot.astropro.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class  CommonUtility {
    public enum CallerFunction {
        GET_LOGIN_DETAIL


    }
    public enum HTTP_REQUEST_TYPE{
        GET,POST
    }
    public enum StoreType {
        NONE, GOOGLE_PLAY
    }
    public static final int RETROFIT_TIMEOUT=30000;

    public static boolean isNetworkAvailable(Context _context) {
        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
