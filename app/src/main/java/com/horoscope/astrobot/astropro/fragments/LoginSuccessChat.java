package com.horoscope.astrobot.astropro.fragments;


import android.app.ProgressDialog;

import android.content.Context;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;

import android.widget.TextView;
import android.widget.Toast;

import com.horoscope.astrobot.astropro.Adapters.DfaultAdapter;
import com.horoscope.astrobot.astropro.Adapters.LoginAdapter;
import com.horoscope.astrobot.astropro.Interface.TimeInterface;
import com.horoscope.astrobot.astropro.Network.RetrofitCall;
import com.horoscope.astrobot.astropro.Network.RetrofitTask;
import com.horoscope.astrobot.astropro.Network.RetrofitTaskListener;
import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.common.CommonUtility;
import com.horoscope.astrobot.astropro.common.ServerConfigStage;

import com.horoscope.astrobot.astropro.model.Default;

import com.horoscope.astrobot.astropro.model.LoginCh;
import com.horoscope.astrobot.astropro.session.SessionManager;


import java.util.concurrent.TimeUnit;

import retrofit.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginSuccessChat extends Fragment implements RetrofitTaskListener<LoginCh>,TimeInterface{
    ProgressDialog progressDialog;


    private RecyclerView mrRecyclerView;
    private  LinearLayoutManager mLinearLayoutManager;
    private LoginAdapter mChatAdapter;
    private DfaultAdapter dChatAdapter;

    private ImageView mSend;
    private EditText mMessageEditText;
    LoginCh mDataList;
    Default lDataList;


    private String chatMsg, astroMsg,email;
    private SessionManager session;
    private TextView timeTxt;
    private RetrofitCall call = new RetrofitCall();
    private long remainTime,timeLeft;
    LoginSuccessChat.CounterClass timer;
    private static final String STARTING_TEXT = "Four Buttons Bottom Navigation";



    public LoginSuccessChat() {
        // Required empty public constructor
    }

    public static LoginSuccessChat newInstance(Context context,int text) {
        Bundle args = new Bundle();
        args.putInt(STARTING_TEXT, text);
        LoginSuccessChat sampleFragment = new LoginSuccessChat();
        sampleFragment.setArguments(args);
        FragmentTransaction ft =((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
        ft.addToBackStack("fragHome");
        ft.commit();


        return sampleFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        session = new SessionManager(getActivity().getApplicationContext());


        getAllId(view);
         allClicks();
        if (session.isLoggedIn()){
            SharedPreferences shared = getActivity().getSharedPreferences("MyPrefs", MODE_PRIVATE);
            email = (shared.getString("emailKey", ""));
            timeTxt.setVisibility(View.VISIBLE);
            callLoginService();


        }else {
            timeTxt.setVisibility(View.GONE);
        }
        return view;
    }

    private void allClicks() {
        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == mSend) {
                    timer = new LoginSuccessChat.CounterClass(remainTime,1000);
                    timer.start();
                    chatMsg = mMessageEditText.getText().toString().trim();
                    if (chatMsg.isEmpty()) {

                    } else {
                             mMessageEditText.setText("");
                            String url = String.format(ServerConfigStage.GET_USER_SUCCESS_LOGIN());
                            RetrofitTask task = new RetrofitTask<LoginCh>(LoginSuccessChat.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.LOGIN_SUCCESS_CHAT, url, getActivity());
                            task.execute();

                    }
                }

            }
        });
    }

    private void getAllId(View view) {
        timeTxt = (TextView)view.findViewById(R.id.time);
        mMessageEditText = (EditText) view.findViewById(R.id.edtvw);
        mrRecyclerView = (RecyclerView) view.findViewById(R.id.recvw);
        mSend = (ImageView) view.findViewById(R.id.btnvw);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mrRecyclerView.setLayoutManager(mLinearLayoutManager);

    }


    public void callLoginService() {
         showProgreass();

        String url = String.format(ServerConfigStage.BY_DEFAULT_RESPONSE());
        RetrofitTask task = new RetrofitTask<LoginCh>(LoginSuccessChat.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.BY_DEFAULT_RES, url, getActivity());
        task.execute();


    }

    @Override
    public void onStart(){
        super.onStart();
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }



    @Override
    public void onRetrofitTaskComplete(Response<LoginCh> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccess()) {
            if (response.body() != null) {
                if (_callerFunction == CommonUtility.CallerFunction.LOGIN_SUCCESS_CHAT) {
                    if (response.body() != null) {


                        mDataList = response.body();


                        if (mChatAdapter == null) {
                            mChatAdapter = new LoginAdapter(getContext(), mDataList, chatMsg, mLinearLayoutManager);
                            mrRecyclerView.setAdapter(mChatAdapter);
                            mChatAdapter.notifyDataSetChanged();

                        }


                    }

                } else if (_callerFunction == CommonUtility.CallerFunction.BY_DEFAULT_RES) {
                    if (response.body() != null) {


                        mDataList = response.body();



                            dChatAdapter = new DfaultAdapter(getContext(), mDataList,  mLinearLayoutManager);
                            mrRecyclerView.setAdapter(dChatAdapter);
                            dChatAdapter.notifyDataSetChanged();




                    }

                }

            }
        }
    }



    @Override
    public void onRetrofitTaskFailure(Throwable t) {

        //stopProgress();
        Toast.makeText(getActivity(), "Fail to load Data", Toast.LENGTH_LONG).show();

        getActivity().finish();

    }

    @Override
    public void onDestroy() {
        stopProgress();
        super.onDestroy();
    }

    @Override
    public void remainingTime(String mFlag, String time) {
        Double d = new Double(time);
        int i = d.intValue();
        remainTime = TimeUnit.MINUTES.toMillis(Long.parseLong(String.valueOf(i)));
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval); }

        @Override public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            timeLeft=millisUntilFinished/1000;
            System.out.println(hms);
            timeTxt.setText(hms);
        }

        @Override public void onFinish() {
            timeTxt.setText("Completed.");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
        timeLeft /= 60;
        String time = Long.toString(timeLeft);
        call.sendTime(getContext(), email, time);
    }


}