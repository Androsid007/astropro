package com.horoscope.astrobot.astropro.Adapters;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.model.LoginCh;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Rahul on 03/02/17.
 */

public class DfaultAdapter extends RecyclerView.Adapter<DfaultAdapter.ViewHolder> {
    Context mContext;
    LoginCh mDataList1;
    LinearLayoutManager mLayoutManager;
    String userMsg;
    TextToSpeech t1;
    ArrayList<LoginCh> anv = new ArrayList<>();





    public DfaultAdapter(Context mContext,  LoginCh mDataList,  LinearLayoutManager mLayoutManager) {
        this.mContext = mContext;
        this.mDataList1 = mDataList;
        this.mLayoutManager = mLayoutManager;
         anv.add(mDataList1);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_chat, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {




        String mString = mDataList1.getReply();
            if (!mString.isEmpty()) {
                holder.astroMsg.setText("ASTRO BOT:-" + mString);

                t1=new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.UK);
                        }
                        if (status == TextToSpeech.SUCCESS){
                            t1.setLanguage(Locale.ENGLISH);
                            // t1.setLanguage(Locale.ENGLISH);
                            String toSpeak =  holder.astroMsg.getText().toString();
                            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                        }
                    }
                });


            }
        }

    @Override
    public int getItemCount() {

        return anv.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView userMsg, astroMsg;

        public ViewHolder(View itemView) {
            super(itemView);
            userMsg = (TextView) itemView.findViewById(R.id.user);
            astroMsg = (TextView) itemView.findViewById(R.id.astrobot);


        }
    }
}