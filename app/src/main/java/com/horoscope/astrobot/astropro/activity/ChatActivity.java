package com.horoscope.astrobot.astropro.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.horoscope.astrobot.astropro.Adapters.ChatAdapter;
import com.horoscope.astrobot.astropro.Interface.MyInterface;
import com.horoscope.astrobot.astropro.Interface.TimeInterface;
import com.horoscope.astrobot.astropro.Network.RetrofitCall;
import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.fragments.Login;
import com.horoscope.astrobot.astropro.model.Balance;
import com.horoscope.astrobot.astropro.model.InformationModel;
import com.horoscope.astrobot.astropro.model.LoginModel;
import com.horoscope.astrobot.astropro.session.SessionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, MyInterface, TimeInterface {

    private RecyclerView mrRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ChatAdapter mChatAdapter;
    private ImageView mSend;
    private EditText mMessageEditText;
    private TextView timeTxt;
    private List<InformationModel> mDataList = new ArrayList<>();
    private String chatMsg, astroMsg;
    private RetrofitCall call = new RetrofitCall();
    private SessionManager session;
    private String email;
    private long remainTime;
    private Toolbar toolbar;
    private Fragment fragment;
    private FragmentTransaction ft;
    private FrameLayout chatFrame,loginFrame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Log.d("Chat Activity","Called");

        session = new SessionManager(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getAllId();
        allClicks();
        if (session.isLoggedIn()){
            timeTxt.setVisibility(View.VISIBLE);
            chatFrame.setVisibility(View.VISIBLE);
            loginFrame.setVisibility(View.GONE);
            email = getIntent().getExtras().getString("email");
            call.checkBalance(this,this,email);

        }else {
            timeTxt.setVisibility(View.GONE);
            chatFrame.setVisibility(View.GONE);
            loginFrame.setVisibility(View.VISIBLE);
            fragment = new Login();
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment).addToBackStack(null).commit();

        }
    }

    private void allClicks() {
        mSend.setOnClickListener(ChatActivity.this);
    }

    private void getAllId() {
        timeTxt = (TextView)findViewById(R.id.time);
        mMessageEditText = (EditText) findViewById(R.id.edtvw);
        mrRecyclerView = (RecyclerView) findViewById(R.id.recvw);
        mSend = (ImageView) findViewById(R.id.btnvw);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mrRecyclerView.setLayoutManager(mLinearLayoutManager);
        mChatAdapter = new ChatAdapter(this, mDataList, chatMsg, mLinearLayoutManager);
        mrRecyclerView.setAdapter(mChatAdapter);
        chatFrame = (FrameLayout)findViewById(R.id.frame);
        loginFrame = (FrameLayout)findViewById(R.id.container);
    }

    @Override
    public void onClick(View v) {
        if (v == mSend) {
            final CounterClass timer = new CounterClass(remainTime,1000);
            timer.start();
            RetrofitCall mRetrofitCall = new RetrofitCall();
            chatMsg = mMessageEditText.getText().toString().trim();
            if (chatMsg.isEmpty()) {

            } else {
                String[] words = chatMsg.split("=");
                if (words.length == 2) {
                    mMessageEditText.setText("");
                    mRetrofitCall.mLogin(this, this, words[0], words[1]);
                    mChatAdapter.notifyDataSetChanged();
                } else if (words.length == 1) {
                    mMessageEditText.setText("");
                    Toast.makeText(this, "Please Enter Correct Name or DOB", Toast.LENGTH_SHORT).show();
                    mChatAdapter.notifyDataSetChanged();
                }
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void myData(String mFlag, Object mObject) {
        InformationModel mInformationModel = (InformationModel) mObject;

        astroMsg = mInformationModel.getName() + "\n" +
                mInformationModel.getDate() + "\n" +
                mInformationModel.getDestiny_number() + "\n" +
                mInformationModel.getRadical_number() + "\n" +
                mInformationModel.getName_number() + "\n" +
                mInformationModel.getEvil_num() + "\n" +
                mInformationModel.getFav_color() + "\n" +
                mInformationModel.getFav_day() + "\n" +
                mInformationModel.getFav_god() + "\n" +
                mInformationModel.getFav_mantra() + "\n" +
                mInformationModel.getFav_metal() + "\n" +
                mInformationModel.getFav_stone() + "\n" +
                mInformationModel.getFav_substone() + "\n" +
                mInformationModel.getFriendly_num() + "\n" +
                mInformationModel.getEvil_num() + "\n" +
                mInformationModel.getRadical_num();

        //  Toast.makeText(getContext(), mString, Toast.LENGTH_SHORT).show();

        mMessageEditText.setText("");
        mDataList.add(mInformationModel);
        mChatAdapter.notifyDataSetChanged();

    }

    @Override
    public void remainingTime(String mFlag, String time) {
        Double d = new Double(time);
        int i = d.intValue();
        remainTime = TimeUnit.MINUTES.toMillis(Long.parseLong(String.valueOf(i)));


    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval); }

       @Override public void onTick(long millisUntilFinished) {
           long millis = millisUntilFinished;
          String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                   TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                   TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
           System.out.println(hms);
           timeTxt.setText(hms);
       }

        @Override public void onFinish() {
            timeTxt.setText("Completed.");
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(ChatActivity.this,NavigationActivity.class);
        startActivity(intent);
    }


}
