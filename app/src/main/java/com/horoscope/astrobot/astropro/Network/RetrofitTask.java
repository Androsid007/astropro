package com.horoscope.astrobot.astropro.Network;

import android.content.Context;
import android.util.Log;


import com.horoscope.astrobot.astropro.common.CommonUtility;
import com.horoscope.astrobot.astropro.common.URLConfig;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;


public class RetrofitTask<M> implements Callback<M> {


    private RetrofitTaskListener<M> _callback;
    private Context _context;

    private CommonUtility.HTTP_REQUEST_TYPE _methodType;
    private CommonUtility.CallerFunction _callerFunction;
    private Object Fast;

    M genricList;

    private String URL;
    private Object postParamsEntity;
    long preExecutionTime, totalExecutionTime;
    boolean shouldAllowRetry = true;
    int timeOut;

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        _context = context;
        this._callerFunction = callerFunction;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
    }

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context, Object postParamsEntity) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
        _context = context;
        this._callerFunction = callerFunction;
        this.postParamsEntity = postParamsEntity;
    }

    public void execute() {
        executeTask(timeOut);
    }


    private void executeTask(int timeOut) {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(timeOut, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(timeOut, TimeUnit.SECONDS);
        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder().url(original.url());
                //.addHeader("User-Agent", "couponlooto_android_app");
                preExecutionTime = System.currentTimeMillis();
                Request request = requestBuilder.build();
                Response response = chain.proceed(request);

                long totalExecutionTime = System.currentTimeMillis() - preExecutionTime;
                Log.i("okhttp", "preExecutionTime " + preExecutionTime);
                Log.i("okhttp", "postExecutionTime" + "" + System.currentTimeMillis() + "\ntotalExecutionTime " + totalExecutionTime + " ms");
                //GoogleAnalyticsConfig.createUserTimingEvent(_context, _callerFunction.name(), totalExecutionTime);
                return response;
            }
        });
        final Retrofit client = new Retrofit.Builder()
                .baseUrl(URLConfig.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        RetrofitApiInterface service = client.create(RetrofitApiInterface.class);

        Call<M> call = null;
        switch (_callerFunction) {
            case GET_SEARCH_CATEGORY_ITEM:
                call = service.getForgetData(URL);
                break;

            case LOGIN_SUCCESS_CHAT:
                call = service.getLoginChat(URL);
                break;

            case BY_DEFAULT_RES:
                call = service.getByDefault(URL);
                break;


        }
        call.enqueue(this);
    }

    @Override
    public void onResponse(retrofit.Response<M> response, Retrofit retrofit) {
        _callback.onRetrofitTaskComplete(response, _context, _callerFunction);
    }

    @Override
    public void onFailure(Throwable t) {
        _callback.onRetrofitTaskFailure(t);
    }
}