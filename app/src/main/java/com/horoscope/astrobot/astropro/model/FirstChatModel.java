package com.horoscope.astrobot.astropro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 2/11/2017.
 */

public class FirstChatModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("query")
    @Expose
    private String query;
    @SerializedName("reply")
    @Expose
    private String reply;
    
    public FirstChatModel(String sta, String quer, String rep) {
        this.status = sta;
        this.query = quer;
        this.reply = rep;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

}
