package com.horoscope.astrobot.astropro.Network;

import android.content.Context;


import com.horoscope.astrobot.astropro.common.CommonUtility;

import retrofit.Response;

public interface RetrofitTaskListener<M> {
    public void onRetrofitTaskComplete(Response<M> response, Context context, CommonUtility.CallerFunction _callerFunction);
    public void onRetrofitTaskFailure(Throwable t);
}
