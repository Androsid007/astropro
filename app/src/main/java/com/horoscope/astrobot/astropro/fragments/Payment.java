package com.horoscope.astrobot.astropro.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.location.Address;
import android.location.Geocoder;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.horoscope.astrobot.astropro.Interface.MyInterface;
import com.horoscope.astrobot.astropro.Network.RequestGenerator;
import com.horoscope.astrobot.astropro.Network.RetrofitCall;
import com.horoscope.astrobot.astropro.R;
import com.horoscope.astrobot.astropro.TrackLocation;
import com.horoscope.astrobot.astropro.activity.CanceledActivity;
import com.horoscope.astrobot.astropro.activity.FailureActivity;
import com.horoscope.astrobot.astropro.activity.SuccessActivity;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.verse.ipayy.crypto.CryptoException;
import in.verse.mpayment.PaymentService;
import in.verse.mpayment.enums.Currency;
import in.verse.mpayment.enums.DiscountType;
import in.verse.mpayment.request.Item;
import in.verse.mpayment.request.ItemDetail;
import in.verse.mpayment.response.FailedPaymentResponse;
import in.verse.mpayment.response.SuccessPaymentResponse;


/**
 * A simple {@link Fragment} subclass.
 */
public class Payment extends Fragment implements AdapterView.OnItemSelectedListener, MyInterface {
    private Spinner country, talkTime;
    private EditText name, email, amount;
    private Button submit;
    private RetrofitCall call = new RetrofitCall();
    private String[] countryArray, timeArray;
    private static final String applicationKey = "p-X6GcWSa4JhnBcOoyL3wg";
    private static final String merchantKey = "8zuueXpmpJCZ6LL5Xce5mA";
    private TextView dateTextView, locationTextView;
    private double latitude, longitude;
    public String address, dateTime, eMail;
    private RetrofitCall retrofitCall;

    public TrackLocation trackLocation;

    public Payment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment, container, false);

        trackLocation = new TrackLocation(getActivity());
        retrofitCall = new RetrofitCall();

        getAndSetParameters(view);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pay();
                //callRegister();
                //openDialog();
            }
        });
        return view;
    }

    private void getAndSetParameters(View view) {
        country = (Spinner) view.findViewById(R.id.country);
        country.setOnItemSelectedListener(this);

        talkTime = (Spinner) view.findViewById(R.id.talkTime);
        talkTime.setOnItemSelectedListener(this);

        name = (EditText) view.findViewById(R.id.name);
        email = (EditText) view.findViewById(R.id.email);
        amount = (EditText) view.findViewById(R.id.amount);

        submit = (Button) view.findViewById(R.id.submit);


        countryArray = getActivity().getResources().getStringArray(R.array.selectCountry);
        timeArray = getActivity().getResources().getStringArray(R.array.selectTime);

        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, countryArray); //selected item will look like a spinner set from XML
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country.setAdapter(countryAdapter);

        ArrayAdapter<String> timeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, timeArray); //selected item will look like a spinner set from XML
        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        talkTime.setAdapter(timeAdapter);

    }

    private void callRegister() {
        String name = this.name.getText().toString().trim();
        eMail = this.email.getText().toString().trim();
        String amount = this.amount.getText().toString().trim();
        String country = this.country.getSelectedItem().toString();
        String talkTime = this.talkTime.getSelectedItem().toString();
        boolean check = call.registerUser(getActivity(), name, eMail, talkTime, amount);
        if (check) {
            openDialog();
        } else {
            openDialog();
            //  Toast.makeText(getContext(), "Registration Failed", Toast.LENGTH_SHORT).show();
        }
    }


    protected void pay() {
        //    callRegister();
        ItemDetail itemDetail = new ItemDetail("ItemTest", "TestGame",
                new BigDecimal("1.0"), BigDecimal.ZERO,
                DiscountType.AMOUNT);
        Item item = new Item(itemDetail, Currency.INR);
        final String requestId = RequestGenerator.getNewRequestId(getActivity());
        Intent paymentIntent = null;
        try {
            paymentIntent = PaymentService.getInstance()
                    .getPaymentIntent(
                            requestId,
                            merchantKey, applicationKey,
                            "9958246062", "Airtel",
                            getActivity(), null, item);
        } catch (CryptoException e) {

            e.printStackTrace();
        }
        startActivityForResult(paymentIntent, 1);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            switch (resultCode) {
                case PaymentService.RESULT_OK: {
                    Intent intent = new Intent(getContext(), SuccessActivity.class);
                    SuccessPaymentResponse successPaymentResponse = (SuccessPaymentResponse) data
                            .getExtras().getSerializable(
                                    PaymentService.SUCCESS_PAYMENT_RESPONSE);
                    Log.d("S",
                            successPaymentResponse.toString());
                    intent.putExtra(PaymentService.SUCCESS_PAYMENT_RESPONSE,
                            successPaymentResponse);
                    callRegister();
                    startActivity(intent);
                    break;
                }
                case PaymentService.RESULT_FAILURE: {
                    Intent intent = new Intent(getContext(), FailureActivity.class);
                    FailedPaymentResponse failedPaymentResponse = (FailedPaymentResponse) data
                            .getExtras().getSerializable(
                                    PaymentService.FAILED_PAYMENT_RESPONSE);
                    Log.d("Failed Payment Response",
                            failedPaymentResponse.toString());
                    intent.putExtra(PaymentService.FAILED_PAYMENT_RESPONSE,
                            failedPaymentResponse);
                    startActivity(intent);
                    break;
                }
                case PaymentService.RESULT_CANCELED: {
                    if (data != null) {
                        Intent intent = new Intent(getContext(), CanceledActivity.class);
                        Item item = (Item) data.getExtras().getSerializable(
                                PaymentService.ITEM);
                        Log.d("Result Cancelled", "Cancelled");
                        intent.putExtra("RESPONSE",
                                "cancelled");
                        intent.putExtra(PaymentService.ITEM, item);
                        startActivity(intent);
                        break;
                    }
                }
                default:
                    break;
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (country.getSelectedItem().toString().equals("India")) {
            if (talkTime.getSelectedItem().toString().equals("10 minutes")) {
                amount.setText(R.string.rs30);
            } else if (talkTime.getSelectedItem().toString().equals("30 minutes")) {
                amount.setText(R.string.rs60);
            }
        } else {
            if (talkTime.getSelectedItem().toString().equals("10 minutes")) {
                amount.setText("$ 1");
            } else if (talkTime.getSelectedItem().toString().equals("30 minutes")) {
                amount.setText("$ 2");
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void openDialog() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View promptView = inflater.inflate(R.layout.register_success, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptView);
        dateTextView = (TextView) promptView.findViewById(R.id.datePickerText);
        locationTextView = (EditText) promptView.findViewById(R.id.locationPickerText);

        final Button dateTime = (Button) promptView.findViewById(R.id.datePicker);
        dateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateTime();

            }
        });
        // final Button loc = (Button) promptView.findViewById(R.id.currentLocation);
        //loc.setOnClickListener(new View.OnClickListener() {
        //  @Override
        //public void onClick(View view) {
        //      try {
        //        findLocation();
        //  } catch (IOException e) {
        //    e.printStackTrace();
        //}
        //    }
        //});

        alertDialogBuilder.setCancelable(false).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                completeRegistration();

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void dateTime() {

        new SingleDateAndTimePickerDialog.Builder(getContext())
                .bottomSheet()
                .curved()
                //.minutesStep(15)
                .mustBeOnFuture()
                .title("Simple")
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {

                        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                        dateTextView.setText(df.format(date));
                        dateTime = df.format(date);

                    }
                }).display();
    }

    private void findLocation() throws IOException {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            if (trackLocation.canGetLocation()) {

                double latitude = trackLocation.getLatitude();
                double longitude = trackLocation.getLongitude();
                //      getAddress(latitude, longitude);
            } else {
                trackLocation.showSettingsAlert();
            }
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//        switch (requestCode) {
//            case 1: {
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    trackLocation = new TrackLocation(getActivity());
//                    if (trackLocation.canGetLocation()) {
//
//                        latitude = trackLocation.getLatitude();
//                        longitude = trackLocation.getLongitude();
//                        try {
//                            getAddress(latitude, longitude);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        trackLocation.showSettingsAlert();
//                    }
//
//                } else {
//
//                }
//                return;
//            }
//
//        }
//    }

    private void getAddress(double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.getDefault());

        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        locationTextView.setText(address + ",\n" + city + " " + postalCode);
        this.address = address + " " + city;
    }

    @Override
    public void myData(String mFlag, Object mObject) {

    }

    private void completeRegistration() {
        address = locationTextView.getText().toString().trim();
        retrofitCall.mSendMessage(getContext(), this, eMail, dateTime, address);

      /*  Fragment fragment = new Home();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment).commit();
   */
        Fragment fragment = new LoginSuccessChat();
        // Fragment fragment = new Home();
        //  ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Astrobot");
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment).commit();
    }
}
