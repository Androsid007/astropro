package com.horoscope.astrobot.astropro.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif on 01/03/17.
 */

public class RemainigTime {
    @SerializedName("EMAIL")
    private String email;

    @SerializedName("MINUTES")
    private String minutes;

    public RemainigTime(String email, String minutes) {
        this.email = email;
        this.minutes = minutes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }
}
