package com.horoscope.astrobot.astropro.Network;


import com.horoscope.astrobot.astropro.model.Demo;
import com.horoscope.astrobot.astropro.model.LoginCh;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Url;


public interface RetrofitApiInterface<M> {
    @GET
    Call<Demo> getForgetData(@Url String url);

    @GET
    Call<LoginCh> getLoginChat(@Url String url);
    @GET
    Call<LoginCh> getByDefault(@Url String url);





}
