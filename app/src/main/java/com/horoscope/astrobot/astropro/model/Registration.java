package com.horoscope.astrobot.astropro.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sidd on 08/02/17.
 */

public class Registration {
    @SerializedName("status")
    private String status;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("talktime")
    private String credit;

    @SerializedName("pay")
    private String amount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Registration(String name, String email, String credit, String amount, String status) {
        this.name = name;
        this.email = email;
        this.credit = credit;
        this.amount = amount;
        this.status=status;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
