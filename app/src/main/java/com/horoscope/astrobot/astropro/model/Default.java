package com.horoscope.astrobot.astropro.model;

/**
 * Created by hp1 on 2/25/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Default {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reply")
    @Expose
    private String reply;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

}